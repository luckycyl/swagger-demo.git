package com.freesky;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class Swagger2Config {
    //swagger2的配置文件
    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                //配置Swagger2包扫描路径
                .apis(RequestHandlerSelectors.basePackage("com.freesky"))
                .paths(PathSelectors.any())
                .build();
    }
    //构建API文档的信息函数
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                //页面标题
                .title("SpringBoot使用Swagger2的DEMO")
                //创建人
                .contact(new Contact("Freesky", "https://blog.csdn.net/qwqw3333333/article/details/84606975", "569265915@qq.com"))
                //版本号
                .version("1.0.0")
                //描述
                .description("这个一个用SpringBoot和Swagger搭建的API")
                .build();
    }
}
