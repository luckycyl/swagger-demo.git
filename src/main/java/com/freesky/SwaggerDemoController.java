package com.freesky;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
@Api("SwaggerDemoController相关的api")
public class SwaggerDemoController {

    private static final Logger logger= LoggerFactory.getLogger(SwaggerDemoController.class);


    /**
     * httpMethod 需要大写
     * @param name
     * @return
     */
    @ApiOperation(value = "对名为 name的人打招呼", httpMethod = "GET")
    @ApiImplicitParam(name = "name", value = "姓名", paramType = "PathVariable", required = true, dataType = "String")
    @GetMapping(value = "/{name}")
    public String hello(@PathVariable String name) {
        logger.info(String.format("开始对[%s]打招呼",name));
        return String.format("你好，%s！",name);
    }
}
